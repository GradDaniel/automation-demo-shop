package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.*;
import static java.lang.String.format;
import static org.fasttrackit.DataSource.HOME_PAGE;

public class Product extends BasePage {

    private final SelenideElement title;
    private final SelenideElement shoppingCartBadge;
    private final SelenideElement mainProductCard;
    private final SelenideElement wishlistButton;
    private final SelenideElement price;


    private final SelenideElement wishlistIcon;
    private final String expectedUrl;
    private final String expectedPrice;
    private final String productId;

    public Product(String productId, String price) {
        this.title = $(format("[href='#/product/%s']", productId));
        this.shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");
        this.mainProductCard = title.parent().parent();
        this.wishlistButton = mainProductCard.$("[data-icon*=heart]");
        this.wishlistIcon = $("[data-icon=heart]~.shopping_cart_badge");
        this.expectedUrl = format("#/product/%s", productId);
        this.productId = productId;
        this.price = title.parent().parent().$(".card-footer .card-text");
        this.expectedPrice = price;
    }

    public String url(){
        return title.getAttribute("href");
    }
    public String productInCartBadge(){
        return shoppingCartBadge.getText();
    }
    public String productInWishlist(){
        return wishlistIcon.getText();
    }
    public String getExpectedUrl(){
        return this.expectedUrl;
    }
    public String getProductId(){
        return productId;
    }
    public String getExpectedPrice(){
        return this.expectedPrice;
    }
    public String getPrice(){
        return this.price.getText();
    }

    public boolean isDisplayed(){
        return title.exists() && title.isDisplayed();
    }
    public boolean isInWishlist(){
        return wishlistButton.getAttribute("data-icon").equals("heart-broken");
    }

    public void addToBasket(){
        SelenideElement parent = title.parent().parent();
        SelenideElement addToBasketIcon = parent.$("[data-icon='cart-plus']");
        addToBasketIcon.click();
    }
    public void addToWishlist(){
        wishlistButton.click();
    }

}
