package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BasePage {
    public void resetApp() {
        if (close.exists())
            close.click();
        $("[data-icon=undo]").click();
    }

    private final SelenideElement close = $(".close");
}
