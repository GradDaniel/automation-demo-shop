package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Modal extends BasePage {

    private final SelenideElement logInModal = $(".modal-content");
    private final SelenideElement helpModal = $(".modal-content");

    public boolean isModalDisplayed(){
        return logInModal.exists() && logInModal.isDisplayed();
    }
    public boolean isHelpModalDisplayed(){
        return helpModal.exists() && helpModal.isDisplayed();
    }
}
