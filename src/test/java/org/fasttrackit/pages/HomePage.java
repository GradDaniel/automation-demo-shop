package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HomePage extends BasePage {

    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final SelenideElement cartIcon = $(".navbar-nav [data-icon='shopping-cart']");
    private final SelenideElement logInIcon = $(".navbar-nav [data-icon='sign-in-alt']");
    private final SelenideElement favoritesIcon = $(".navbar-nav [data-icon='heart']");
    private final SelenideElement questionIcon = $(".fa-question");


    public String logoRedirectURL() {
        return logo.parent().getAttribute("href");
    }

    public String greetingsMessage() {
        return greetings.getText();
    }

    public String cartIconURL() {
        return cartIcon.parent().getAttribute("href");
    }

    public String wishlistRedirectUrl() {
        return favoritesIcon.parent().getAttribute("href");
    }

    public boolean isLogoDisplayed(){
        return logo.exists() && logo.isDisplayed();
    }
    public boolean isGreetingMessageDisplayed(){
        return greetings.exists() && greetings.isDisplayed();
    }
    public boolean isCartIconDisplayed(){
        return cartIcon.exists() && cartIcon.isDisplayed();
    }
    public boolean isLogInIconDisplayed(){
        return logInIcon.exists() && logInIcon.isDisplayed();
    }
    public boolean isFavoriteIconDisplayed(){
        return favoritesIcon.exists() && favoritesIcon.isDisplayed();
    }
    public boolean isQuestionIconDisplayed(){
        return questionIcon.exists() && questionIcon.isDisplayed();
    }

    public void questionModalOpen(){
        questionIcon.click();
    }

    public void logInIconOpen(){
        logInIcon.click();
    }
}
