package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

public class AutomationDemoShopTest {

    HomePage homePage;
    Modal modal;
    DataSource dataSource;
    BasePage basePage;
    Product product;

    @BeforeTest
    public void before(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        //SelenideLogger.addListener("AllureSelenide", listener);
        open(HOME_PAGE);
        homePage = new HomePage();
        modal = new Modal();
        dataSource = new DataSource();
        basePage = new BasePage();
    }

    @AfterMethod
    public void cleanup() {
        homePage.resetApp();
    }

    @Test
    public void verify_homepage_logo(){
        assertTrue(homePage.isLogoDisplayed(),  "Logo element exist in navigation bar");
        assertEquals(homePage.logoRedirectURL(), HOME_PAGE,    "Clicking the logo redirect to homepage");
    }

    @Test
    public void verify_greeting_message(){
        assertTrue(homePage.isGreetingMessageDisplayed());
        assertEquals(homePage.greetingsMessage(),  HELLO_GUEST_GREETINGS_MSG, "Welcome message is displayed.");
    }

    @Test
    public void verify_cart_icon(){
        assertTrue(homePage.isCartIconDisplayed(), "Cart icon is displayed");
        assertEquals(homePage.cartIconURL(), HOME_PAGE + "#/cart", "Page redirect to your cart");
    }

    @Test
    public void  verify_wishlist_icon(){
        assertTrue(homePage.isFavoriteIconDisplayed(), "Wishlist icon is displayed");
        assertEquals(homePage.wishlistRedirectUrl(), HOME_PAGE + "#/wishlist",
                "Page redirect to wishlist");
    }

    @Test
    public void verify_login_icon(){
        assertTrue(homePage.isLogInIconDisplayed(), "Login icon is displayed");
        homePage.logInIconOpen();
//        sleep(10*1000);
        assertTrue(modal.isModalDisplayed(), "After clicking on login button login modal is displayed");
    }

    @Test
    public void verify_question_button_opens_help_modal(){
        assertTrue(homePage.isQuestionIconDisplayed(), "Question icon is displayed");
        homePage.questionModalOpen();
        assertTrue(modal.isHelpModalDisplayed(), "After clicking on help button help modal is displayed");
    }

    @Test
    public void cart_icon_updates_upon_adding_a_product_to_cart(){
        product = AWESOME_GRANITE_CHIPS_PRODUCT;
        product.addToBasket();
        assertEquals(product.productInCartBadge(), "1",
                "After adding product to basket cart icon is updated");
    }

    @Test
    public void wishlist_icon_updates_upon_adding_a_product_to_wishlist(){
        product = AWESOME_METAL_CHAIR_PRODUCT;
        product.addToWishlist();
        assertTrue(product.isInWishlist(),
                "After adding a product to wishlist, the wishlist icon crashes");
        assertEquals(product.productInWishlist(), "1",
                "After adding a product to wishlist, badge updates");
    }

    @Test
    public void verify_products_sort_mode() {
        ElementsCollection allProducts = $$(".card");
        SelenideElement firstProduct = allProducts.first();
        String firstProductTitle = firstProduct.getText();
        SelenideElement sortMode = $(".sort-products-select");
        assertTrue(sortMode.exists(), "Sort modes is displayed");
        sortMode.click();
        assertTrue(sortMode.isDisplayed(), "Sort type is displayed");
        SelenideElement highToLow = $("option[value=hilo]");
        highToLow.click();
        sortMode.click();
        ElementsCollection allProductsAfterSort = $$(".card");
        SelenideElement firsProductAfterSort = allProductsAfterSort.first();
        String fistProductTitleAfterSort = firsProductAfterSort.getText();
        assertNotEquals(firstProductTitle, fistProductTitleAfterSort);
    }

    @Test(dataProvider = "getProductsData", dataProviderClass = DataSource.class)
    public void verify_productsURL(Product products){
        assertTrue(products.isDisplayed(),"Product" + products.getProductId() +  "is displayed on page");
        assertEquals(products.url(),HOME_PAGE + products.getExpectedUrl(),
                "Product " + products.getProductId() + " page is open");
        assertEquals(products.getPrice(), products.getExpectedPrice(),
                "Product" +products.getProductId() + "price is" + products.getExpectedPrice());
    }

    @Test(dataProvider = "getProductsDataWithWrongPrice", dataProviderClass = DataSource.class)
    public void verify_productsURLandFailedPrice(Product products){
        assertTrue(products.isDisplayed(),"Product" + products.getProductId() +  "is displayed on page");
        assertEquals(products.url(),HOME_PAGE + products.getExpectedUrl(),
                "Product " + products.getProductId() + " page is open");
        assertNotEquals(products.getPrice(), products.getExpectedPrice(),
                "Product" +products.getProductId() + "price is" + products.getExpectedPrice());
    }
}
