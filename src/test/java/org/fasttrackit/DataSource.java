package org.fasttrackit;

import org.fasttrackit.pages.BasePage;
import org.fasttrackit.pages.Product;
import org.testng.annotations.DataProvider;

public class DataSource extends BasePage {
    public static final String HELLO_GUEST_GREETINGS_MSG = "Hello guest!";
    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    public static final String REFINED_FROZEN_MOUSE_PRODUCT_ID = "0";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT_ID = "1";
    public static final String INCREDIBLE_CONCRETE_HAT_PRODUCT_ID = "2";
    public static final String AWESOME_METAL_CHAIR_PRODUCT_ID = "3";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_ID = "4";
    public static final String AWESOME_SOFT_SHIRT_PRODUCT_ID = "5";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_2_ID = "6";
    public static final String PRACTICAL_METAL_MOUSE_PRODUCT_ID = "7";
    public static final String LICENSED_STEEL_GLOVES_PRODUCT_ID = "8";
    public static final String GORGEOUS_SOFT_PIZZA_PRODUCT_ID = "9";

    public static final Product REFINED_FROZEN_MOUSE_PRODUCT = new Product(REFINED_FROZEN_MOUSE_PRODUCT_ID, "$9.99");
    public static final Product AWESOME_GRANITE_CHIPS_PRODUCT = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID, "$15.99");
    public static final Product INCREDIBLE_CONCRETE_HAT_PRODUCT = new Product(INCREDIBLE_CONCRETE_HAT_PRODUCT_ID, "$7.99");
    public static final Product AWESOME_METAL_CHAIR_PRODUCT = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID, "$15.99");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_ID,"$29.99");
    public static final Product AWESOME_SOFT_SHIRT_PRODUCT = new Product(AWESOME_SOFT_SHIRT_PRODUCT_ID, "$29.99");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT_2 = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_2_ID, "$1.99");
    public static final Product PRACTICAL_METAL_MOUSE_PRODUCT = new Product(PRACTICAL_METAL_MOUSE_PRODUCT_ID, "$9.99");
    public static final Product LICENSED_STEEL_GLOVES_PRODUCT = new Product(LICENSED_STEEL_GLOVES_PRODUCT_ID, "$14.99");
    public static final Product GORGEOUS_SOFT_PIZZA_PRODUCT = new Product(GORGEOUS_SOFT_PIZZA_PRODUCT_ID, "$19.99");

    public static final Product REFINED_FROZEN_MOUSE_PRODUCT_WRONG_PRICE = new Product(REFINED_FROZEN_MOUSE_PRODUCT_ID, "$19.99");
    public static final Product AWESOME_GRANITE_CHIPS_PRODUCT_WRONG_PRICE = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID, "$115.99");
    public static final Product INCREDIBLE_CONCRETE_HAT_PRODUCT_WRONG_PRICE = new Product(INCREDIBLE_CONCRETE_HAT_PRODUCT_ID, "$17.99");
    public static final Product AWESOME_METAL_CHAIR_PRODUCT_WRONG_PRICE = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID, "$115.99");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT_WRONG_PRICE = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_ID,"$129.99");
    public static final Product AWESOME_SOFT_SHIRT_PRODUCT_WRONG_PRICE = new Product(AWESOME_SOFT_SHIRT_PRODUCT_ID, "$129.99");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT_2_WRONG_PRICE = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_2_ID, "$11.99");
    public static final Product PRACTICAL_METAL_MOUSE_PRODUCT_WRONG_PRICE = new Product(PRACTICAL_METAL_MOUSE_PRODUCT_ID, "$19.99");
    public static final Product LICENSED_STEEL_GLOVES_PRODUCT_WRONG_PRICE = new Product(LICENSED_STEEL_GLOVES_PRODUCT_ID, "$114.99");
    public static final Product GORGEOUS_SOFT_PIZZA_PRODUCT_WRONG_PRICE = new Product(GORGEOUS_SOFT_PIZZA_PRODUCT_ID, "$119.99");

    @DataProvider(name = "getProductsData")
    public static Object[][] getProductsData(){
        Object[][] product = new Object[10][];
        product[0] = new Object[]{REFINED_FROZEN_MOUSE_PRODUCT};
        product[1] = new Object[]{AWESOME_GRANITE_CHIPS_PRODUCT};
        product[2] = new Object[]{INCREDIBLE_CONCRETE_HAT_PRODUCT};
        product[3] = new Object[]{AWESOME_METAL_CHAIR_PRODUCT};
        product[4] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT};
        product[5] = new Object[]{AWESOME_SOFT_SHIRT_PRODUCT};
        product[6] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT_2};
        product[7] = new Object[]{PRACTICAL_METAL_MOUSE_PRODUCT};
        product[8] = new Object[]{LICENSED_STEEL_GLOVES_PRODUCT};
        product[9] = new Object[]{GORGEOUS_SOFT_PIZZA_PRODUCT};
        return product;
    }

    @DataProvider(name = "getProductsDataWithWrongPrice")
    public static Object[][] verify_productsURLandFailedPrice(){
        Object[][] product = new Object[10][];
        product[0] = new Object[]{REFINED_FROZEN_MOUSE_PRODUCT_WRONG_PRICE};
        product[1] = new Object[]{AWESOME_GRANITE_CHIPS_PRODUCT_WRONG_PRICE};
        product[2] = new Object[]{INCREDIBLE_CONCRETE_HAT_PRODUCT_WRONG_PRICE};
        product[3] = new Object[]{AWESOME_METAL_CHAIR_PRODUCT_WRONG_PRICE};
        product[4] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT_WRONG_PRICE};
        product[5] = new Object[]{AWESOME_SOFT_SHIRT_PRODUCT_WRONG_PRICE};
        product[6] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT_2_WRONG_PRICE};
        product[7] = new Object[]{PRACTICAL_METAL_MOUSE_PRODUCT_WRONG_PRICE};
        product[8] = new Object[]{LICENSED_STEEL_GLOVES_PRODUCT_WRONG_PRICE};
        product[9] = new Object[]{GORGEOUS_SOFT_PIZZA_PRODUCT_WRONG_PRICE};
        return product;
    }
}