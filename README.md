# Demo Shop Test Automation Framework
A brief introduction of my project


## This is the final project for Grad Daniel within the FastTrackIt Test Automation course


### How to run tests
`https://gitlab.com/GradDaniel/automation-demo-shop.git`

#### Execute all test
`mvn clean test`

`mvt allure:report`

`mvn allure:serve`

#### Page Objects
    -BasePage
    -HomePage
    -Modal
    -Products

#### Test implemented
| Test Name                             | Execution Status | Date       |
|---------------------------------------|------------------|------------|
| Verify Greetings Message              | **PASSED**       | 10.04.2022 |
| Verify Homepage Logo                  | **PASSED**       | 10.04.2022 |
| Verify Cart Icon                      | **PASSED**       | 10.04.2022 |
| Verify Wishlist Icon                  | **PASSED**       | 10.04.2022 |
| Verify LogIn Icon                     | **PASSED**       | 10.04.2022 |
| Verify Question Icon                  | **PASSED**       | 10.04.2022 |
| Cart Icon Update                      | **PASSED**       | 10.04.2022 |
| Wishlist Icon Update                  | **PASSED**       | 10.04.2022 |
| Verify Product Sort Mode              | **PASSED**       | 10.04.2022 |
| Verify Products Url                   | **PASSED**       | 10.04.2022 |
| Verify Product Price                  | **PASSED**       | 10.04.2022 |
| Verify Product Price With Wrong Price | **PASSED**       | 10.04.2022 |



